# Obfuscated Text Classification Problem

Welcome to my solution to the obfuscated text classifcation problem. 

## Problem Description

Which Novel Do I Belong To?

In this task, you are expected to learn a Machine Learning model that 
classifies a given line as belonging to one of the following 12 novels:

0.  Alice's Adventures in Wonderland
1.  Dracula
2.  Dubliners
3.  Great Expectations
4.  Hard Times: For These Times
5.  Adventures of Huckleberry Finn
6.  Les Miserables
7.  Moby-Dick
8.  Oliver Twist
9.  Peter Pan
10.  A Tale of Two Cities
11.  The Adventures of Tom Sawyer

You are provided with a zip file (offline_challenge.zip) containing 
three text files - xtrain.txt, ytrain.txt, xtest.txt. Each line in 
xtrain.txt and xtest.txt comes from a different novel. The data has 
been obfuscated, however the patterns in them are preserved. The novel 
ids corresponding to xtrain.txt are specified in ytrain.txt. You can 
use these labels to train a Machine Learning model (Deep Learning 
preferred).

With the learned model, predict the novel ids of the lines in 
xtest.txt (one prediction per line). As part of your submission, 
include

*  Your predictions (in the same format as ytrain.txt)
*  Expected accuracy on the test set
*  The source code for training and prediction (< 10MB)
*  A brief description of your method (optional)

## My Solution

*  The folder best_weights contains the weights for various modules
I trained and the predictions on the test set for some of them.
The five digits in the filename give the expected accuracy 
and allow you to match a model with its prediction file.

*  The expected accuracy of my best model is roughly 86% as this 
was the accuracy on the cross-validation set (on which my model 
has never been trained).

*  The source code can be found in model.py. It should be noted
that the code was written to solve the task, but not with
usability in mind. 

*  When calling ```python model.py```, my model is compiled and trained
for 60 epochs. In addition, the weights with the highest accuracy
on the cross-validation set during the training phase are used to
make the predictions on the test set. In order to use my
pretrained model, you need to set the variable ```best_model_filename```
to the path of the downloaded file in model.py.
 
*  I described my approach to the obfuscated text classification
problem in model_description\model_desc.pdf.

*  The Jupyter notebook data_exploration.ipynb compares
the chosen model with other models.


