\documentclass[a4paper, 10pt]{article}

%\overfullrule=1mm

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage{lmodern}  % set of high quality outline fonts called Latin Modern
%\usepackage{pxfonts} % based on Palatino as its main text body font
%\usepackage{txfonts} % based on Times as its main text body font
\usepackage[english]{babel}
\usepackage{csquotes} % ensure that quoted texts are typeset according to the rules of the main language

%\usepackage[scale=0.73, a4paper]{geometry}
\usepackage{textcomp} % provides extra symbols
\usepackage{graphicx} % provides resizebox command to resize large
                      % xypic-diagrams
\usepackage{amsmath} % general maths extension
\usepackage{amsthm} % provides improved theorem environments
\usepackage{amssymb} % provides symbols and fonts
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{thmtools} % provides flexible reference for thm environments and other cool features
\usepackage{mathrsfs} % provides the command \mathscr
\usepackage{mathdots} % needed for \iddots (inverse diagonal dots)
%\usepackage{setspace}
%\usepackage{MnSymbol} % provides dotcup and other mathematical symbols
\usepackage{wasysym} % needed for a prettier command replacing \emptyset
%\usepackage{pdflscape} % needed to make certain pages landscape 
                       % using \begin{landscape} ... \end{landscape}
\usepackage{extpfeil} % provides additional arrows
\usepackage{verbatim} % provides multiline comments
\usepackage{longtable} % for tables spanning multiple pages
\usepackage{bigstrut} % needed to add extra vertical space in rows of tabular 
                      % environments with super- or subscripts
\usepackage[table, dvipsnames]{xcolor} % for colouring cells in tables
\usepackage{floatpag} % needed to change the pagestyle on a floatpage
\usepackage{enumitem} % needed to change the indentation in the description environment
%\usepackage{afterpage} % to place longtables on the next page using \afterpage
%\usepackage{placeins} % needed to place all floats using \FloatBarrier without starting a new page
%\usepackage{leftidx} % provides left super- und subscripts
%\usepackage{sfrac} % needed for slanted fractions \sfrac
\usepackage{xparse} % needed to define commands with more than one optional argument
\usepackage{mathscinet} % needed for \germ and other commands sometimes used
                        % in bibtex entries created by MathSciNet
\usepackage{adjustbox} % used to resize tables if they are too large
\usepackage[obeyDraft]{todonotes} % provides to-do lists and todonotes

\usepackage[final,                  % overwrite possible draft option in documentclass
            %pdftex,                 % use pdftex
            %pdfpagelabels,
            %pdfstartview = {FitH},  % fit the width of the page to the window
            %bookmarks,              % show the bookmarks bar when displaying the document 
            colorlinks,             % colour the text of the links instead of using a coloured box
            plainpages = false,     % make page anchors using the formatted form of the page number 
                                    % to differentiate ii and 2 
            linktoc=all,            % set to all if you want both sections and subsections linked
            linkcolor=black,        % colour for links
            citecolor=black,         % colour for citations
            urlcolor=black,          % colour for URL links
            filecolor=black         % colour for file links
            ]{hyperref}

\usepackage[citestyle=alphabetic,   % citation style
            bibstyle=alphabetic,    % bibliography style<}_{#2}
            maxbibnames=50,         % maximal number of names displayed in the bibliography
            maxcitenames=3,         % maximal number of names displayed in a citation
            autocite=inline,        % defines the standard for \autocite (plain=\cite, inline=\parancite, ...)
            block=space,            % determines the spacing between larger segments of a bibliography entry
            hyperref=true,          % transfrom citations into clickable hyperlinks or not
            backref=false,           % print back references in the bibliography or not
            backrefstyle=three,     % controls how sequences of consecutive pages in the list of back references are formatted
            date=short,             % controls the basic format of printed dates
            arxiv=pdf,              % path selector for arXiv links
            isbn=false,             % display ISBN number or not
            url=false,              % display URL or not
            doi=false,               % display the field DOI or not
            eprint=true,            % display the field EPRINT or not
            giveninits=true,        % all first and middle names will be rendered as initials or not
            sorting=nyt,            % sort by name, title, year
            backend=bibtex8,
            ]{biblatex}

% Fix the penalty for breaking URLS and DOIs in the bibliography
% in order to prevent long URLs from running into the margin
\setcounter{biburlnumpenalty}{9000}
\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}
            

% TODO: Solve the problem with the pdfkeywords!
\hypersetup{pdftitle = {Model Description},
            pdfauthor = {Lars Thorge Jensen},
            pdfkeywords = {multiclass classification} {obfuscated text},
            pdfdisplaydoctitle      % display document title instead of file name in title bar 
            }
            
\usepackage[arrow, matrix, curve]{xy} % provides commutative diagrams
\usepackage{tikz} % used for digrammatics
\usetikzlibrary{arrows, arrows.meta, bending, fit, intersections, calc, external, shapes.misc, shapes.geometric, decorations.markings, decorations.pathreplacing}
\usepackage{pgfplots}
\pgfplotsset{compat=1.14}

%\usepackage{tkz-euclide}
%\usetkzobj{all}

% Get Roman numbers for enumerations 
\renewcommand{\labelenumi}{\textup{(\roman{enumi})}}

\newcommand{\Address}{
  \bigskip{\footnotesize
  %\textsc{Max Planck Institute for Mathematics, Vivatsgasse 7, 53111 Bonn}\par\nopagebreak
  \textsc{Universit\'{e} Clermont Auvergne, CNRS, LMBP, F-63000 Clermont-Ferrand, France}\par\nopagebreak
  \textit{E-mail address}, Lars~Thorge~Jensen: \texttt{lars\_thorge.jensen@uca.fr}
}}

\pagestyle{headings}

\begin{document}

\title{Approach to the Obfuscated Text Classification Problem}
\author{Lars Thorge Jensen}
\date{} % This leads to the omission of the date!

\maketitle

My approach is motivated by the paper ``Character-level Convolutional Networks for
Text Classification'' written by Xiang Zhang, Jumbo Zhao and Yann LeCun because
the usual NLP pipeline using word embeddings and self-attention models is not
immediately applicable due to the obfuscation.

\section{Data Preparation}

Since we are facing a multinomial classification problem with 12 classes, we first transform
the novel ids ranging from $0$ to $11$ to one-hot encodings such that the novel id 
$i$ corresponds to a the vector with a $1$ in the $(i+1)$-st position and all other entries 
set to $0$:
\[ 0 \mapsto \begin{pmatrix} 1 \\ 0 \\ \vdots \\ 0 \end{pmatrix}, \quad
   1 \mapsto \begin{pmatrix} 0 \\ 1 \\ 0 \\ \vdots \\ 0 \end{pmatrix}, \quad \dots, \quad
   11 \mapsto \begin{pmatrix} 0 \\ \vdots \\ 0 \\ 1 \end{pmatrix}\]

It is easy to verify that all characters occurring in the obfuscated sentences are lowercase
letters from ``a'' to ``z''. The longest sentence in the dataset consists of $452$
characters. Therefore, we transform the obfuscated sentences to sequences of one-hot
encodings of the letters ``a'' to ``z'' and pad them with zeros so that all sequences
have the same length. In our one-hot encodings the $i$-th letter in the alphabet will
correspond to the vector with a $1$ in the $i$-th position and all other entries set to $0$.

After that we randomly split the labelled data set into a training set and a cross-validation
set such that the training set contains $80\%$ of the $32513$ labelled sentences.

\section{Model Description}

After experimenting with various network architectures (see the attached Jupyter notebook),
we have chosen the following model:

The idea is to use 2d convolutional layers to search for character patterns.
Recall that each lowercase letter corresponds to a dimension in the one-hot encoding,
so that each sentence is encoded as a sequence (of length $452$) of $26$-dimensional
one-hot encodings. 

In the model the input is fed into several parallel convolutional blocks for various kernel
sizes $k \in \{4,5,6,7,8,9\}$: Each block consists of a 2d convolutional
layer with kernel size $(k, 26)$ and $128$ filters, followed by a max pooling
layer with pool size $(452 - k + 1, 1)$ (i.e. over the remaining sentence length), 
a 2d spatial dropout and a flatten layer. 

The motivation for this architecture is as follows: 
By combining several convolutional blocks we achieve a higher accuracy
on the cross-validation set. The 2d convolutional layer in each block
is trained to recognize patterns of $k$ consecutive letters in the sentence. 
By applying the max pooling layer only the most prominent pattern in the whole 
sentence is retained for each filter. Dropout is used to prevent overfitting 
and flatten allows to reshape a tensor of shape $(1, 1, 128)$ to one of shape $(128)$.

The output tensors of all the convolutional blocks are concatenated
and fed into two consecutive fully connected layers with first 
$6 * 128 = 768$ and then $128$ neurons and ReLU activation function.
To both layers, dropout is applied to prevent overfitting.

Since we have a multinomial classification problem with $12$ classes, the output
layer is another fully-connected layer with $12$ neurons. The softmax
activation function ensures that the $12$ output values lie in the
interval $[0, 1]$ and sum to $1$. Therefore, the $i$-th output value
can be interpreted as the probability predicting whether the given sample
has novel id $i-1$.

As loss function we use the categorical crossentropy which is suitable for
the multinomial classification problem. Since one of the tasks is to predict
the accuracy on the test set, we use the categorical accuracy as metric.

\section{Expected Accuracy on the Test Set}

Our model achieved an accuracy of roughly $86\%$ on the cross-validation
set (on which it has never been trained). For that reason, we expect a similar
accuracy on the test set.

\section{Future Directions}

Due to my time constraints and my limited computational resources, I tweaked 
the hyperparameters (dropout rates, kernel sizes for convolutional layers, number 
of neurons per layer, etc.) to some extent, but there is still room for improvement.
The next step would be to use Bayesian optimization for the hyperparameters.

\end{document}