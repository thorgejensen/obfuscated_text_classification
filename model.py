import sys
import numpy as np
from sklearn.model_selection import train_test_split
from keras.models import Model, load_model
#from keras.regularizers import l2
from keras.layers import Input, Conv2D, MaxPooling2D, SpatialDropout2D, Reshape, Flatten, Concatenate, Dense, Dropout, BatchNormalization
#from keras.optimizers import RMSprop
from keras.callbacks import ModelCheckpoint
from keras.utils import to_categorical

# The code was written to solve the given task, not with usability in mind.
# Modify the following variables if the necessary files are not in the same folder.
X_train_filename = 'xtrain_obfuscated.txt'
X_test_filename = 'xtest_obfuscated.txt'
y_train_filename = 'ytrain.txt'
best_model_filename = 'model_best.tf'
numEpochs = 60
maxChar = 26
numClasses = 12

def prepareTrainData(X_filename, y_filename):
    """
    Loads the sentences from X_filename and the novel ids from y_filename.
    Transforms both to one-hot encodings and prepares the data for training.
    
    Args:
       X_filename (string): Name of text file containing the obfuscated sentences
       y_filename (string): Name of text file containing the novel ids
       
    Return values:
        X (numpy.array of shape (numSentences, maxLen, maxChar, 1)): One-hot encoding of obfuscated sentences
        y (numpy.array of shape (numSentences, numClasses)): One-hot encoding of novel ids
        maxLen (integer): Maximal sentence length
    """
    
    # Load data
    try:
        with open(X_filename, 'r') as fo:
            X_alpha = [line.strip('\n') for line in fo]
            
        with open(y_filename, 'r') as fo:
            y_int = [int(line.strip('\n')) for line in fo]
    except OSError:
        print(f"We cannot open {X_filename} or {y_filename}!")
        return [], [], 0
    except:
        print('Unexpected error:', sys.exc_info()[0])
        raise

    # Determine the maximal length of a sentence
    maxLen = 0
    for sent in X_alpha:
        curLen = len(sent)        
        if curLen > maxLen:
            maxLen = curLen
    
    # Transform y to a binary one-hot encoding
    y = to_categorical(y_int, num_classes=numClasses)

    # Transform X to a binary one-hot encoding and pad all sequences with zeros
    X = [np.zeros((maxLen, maxChar)) for x in X_alpha]
    for i in range(len(X_alpha)):
        for j in range(len(X_alpha[i])):
            X[i][j][ord(X_alpha[i][j]) - 97] = 1
    X = np.asarray(X, dtype=np.int)

    # Reshape X
    X = X.reshape((X.shape[0], X.shape[1], X.shape[2], 1))
    
    return X, y, maxLen

def prepareTestData(maxLen, X_filename = X_test_filename):
    """
    Loads the obfuscated sentences from X_filename and transforms them to one-hot encodings.
    
    Args:
       maxLen (integer): Maximal sentence length
       X_filename (string): Name of text file containing the obfuscated sentences
       
    Return values:
        X (numpy.array of shape (numSentences, maxLen, maxChar, 1)): Test set of one-hot encodings of obfuscated sentences
    """
    
    # Load data
    try:
        with open(X_filename, 'r') as fo:
            X_alpha = [line.strip('\n') for line in fo]
    except OSError:
        print(f"We cannot open {X_filename}!")
        return []
    except:
        print('Unexpected error:', sys.exc_info()[0])
        raise

    # Transform X_alpha to a binary one-hot encoding and pad all sequences with zeros
    X = [np.zeros((maxLen, maxChar)) for x in X_alpha]
    for i in range(len(X_alpha)):
        for j in range(len(X_alpha[i])):
            X[i][j][ord(X_alpha[i][j]) - 97] = 1
    X = np.asarray(X, dtype=np.int)

    # Reshape X
    X = X.reshape((X.shape[0], X.shape[1], X.shape[2], 1))
    
    return X

def compileModel(maxLen, convSizes = [5, 7, 9], numFilters = 128, verbose = True):
    """
    Compiles the Keras model consisting of several parallel 2D convolutional layers 
    to detect spatial patterns followed by batch normalization and temporal
    max pooling layers. The output of these parallel stacks will be flattened and
    concatenated before it is fed into two final fully-connected layers to which
    we apply dropout to avoid overfitting.
    
    Args:
       maxLen (integer): Maximal sentence length
       convSizees (list of integers): The temporal kernel_sizes used in the convolutional layers
       numFilters (integer): Number of filters used in the convolutional layers
       verbose (boolean): Determines whether a summary of the model is printed
       
    Return values:
        my_model (tf.keras.Model): Compiled Keras model
    """

    inputs = Input(shape=[maxLen, maxChar, 1])

    convBlocks = []
    
    for convSize in convSizes: 
        # Use convolutional layers to detect spatial patterns
        # Dropout works better than l2-regularization
        # four layers + l2(0.001) --> overfitting, l2(0.01) was better, but dropout performs better
        conv = Conv2D(filters=numFilters, kernel_size=(convSize, maxChar))(inputs)#, kernel_regularizer=l2(0.01))(inputs)
        
        # Batch Normalization
        # Performance was better without this
        #batchnorm = BatchNormalization()(conv)
        
        # Temporal Max Pooling (over the remaining sentence length)
        maxpool = MaxPooling2D(pool_size=(maxLen - convSize + 1, 1))(conv)
        
        # Dropout to prevent overfitting
        drop = SpatialDropout2D(0.55)(maxpool)
    
        # Flatten
        flatten = Flatten()(drop)
        
        convBlocks.append(flatten)
        
    # Merge all the flattened layers
    merged = Concatenate(axis=1)(convBlocks)

    # Two fully-connected layers with dropout applied to them
    dense1 = Dense(len(convBlocks) * numFilters, activation='relu')(merged)
    drop1 = Dropout(0.5)(dense1)
    dense2 = Dense(128, activation='relu')(drop1)
    drop2 = Dropout(0.5)(dense2)

    outputs = Dense(numClasses, activation='softmax')(drop2)

    # Compile the model and print a summary (if verbose is set to True)
    my_model = Model(inputs=inputs, outputs=outputs)
    # Internally Keras uses categorical_accuracy whenever the loss function is categorical_crossentropy and accuracy is specified as metric
    my_model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['categorical_accuracy'])
    if verbose:
        my_model.summary()
        
    return my_model
    
def train(my_model, X_train, X_val, y_train, y_val, batchSize = 128, numEpochs = 20, useCheckpoint = True, filename = 'model_best.tf'):
    """
    Trains the given Keras model on the training set and evalutes its categorical accuracy
    on the given cross-validation set.
    
    Args:
       my_model (tf.keras.Model): Compiled Keras model
       X_train (numpy.array of shape (numSentences, maxLen, maxChar, 1)): Training data consisting of one-hot encodings of obfuscated sentences
       X_val (numpy.array of shape (numSentences, maxLen, maxChar, 1)): Cross-validation data consisting of one-hot encodings of obfuscated sentences
       y_train (numpy.array of shape (numSentences, numClasses)): Training labels consisting of one-hot encodings of novel ids
       y_val (numpy.array of shape (numSentences, numClasses)): Cross-validation data consisting of one-hot encodings of novel ids
       batchSize (integer): Batch size used for training
       numEpochs (integer): Number of epochs the model will be trained for
       useCheckpoint (boolean): Determines whether the parameters of the best model (based on val_accuracy) will be saved to the given file
       filename (string): In this file the parameters of the best model will be saved if useCheckpoint is set to True
    """
    
    if useCheckpoint:
        checkpoint = ModelCheckpoint(filename, monitor='val_categorical_accuracy', save_best_only=True, mode='max', verbose=1)
        callbackList = [checkpoint]
    else:
        callbackList = []

    print('Training the model')
    history = my_model.fit(X_train, y_train, batch_size=batchSize, epochs=numEpochs, shuffle=True, validation_data=(X_val, y_val), callbacks=callbackList)

def predict(X_test, model_file="model_best.tf", out_file="ytest.txt"):
    """
    Predicts the labels on X_test using the Keras model stored in model_file
    and writes them to the out_file (one per line).
    
    Args:
       X_test (numpy.array of shape (numSentences, maxLen, maxChar, 1)): Test data consisting of one-hot encodings of obfuscated sentences
       model_file (string): File containing the parameters of the Keras model
       out_file (string): File to which the predictions are written
       
    Return values:
       y_pred (list of integers): Novel ids predicted by the given Keras model
    """
    my_model = load_model(model_file)
    
    # Predict the labels for X_test
    y_pred_probs = my_model.predict(X_test)
    y_pred = np.argmax(y_pred_probs, axis=1)
    
    # Write predictíons to out_file
    with open(out_file, 'w') as fo:
        fo.writelines("%s\n" % y for y in y_pred)

    return y_pred
        
X, y, maxLen  = prepareTrainData(X_train_filename, y_train_filename)
if (maxLen > 0) and (len(X) == len(y)):
    # Randomly split the given data into a training and a cross-validation set
    X_train, X_val, y_train, y_val = train_test_split(X, y, test_size=0.2)
    
    # Compile and train our model
    my_model = compileModel(maxLen, convSizes = [4, 5])
    #my_model = load_model(best_model_filename)
    train(my_model, X_train, X_val, y_train, y_val, numEpochs = numEpochs)
    
    X_test = prepareTestData(maxLen)
    if len(X_test) > 0:
        predict(X_test, model_file=best_model_filename)
    